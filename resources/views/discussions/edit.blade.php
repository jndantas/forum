@extends('layouts.app')

@section('content')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Editar uma Discussão</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <form action="{{ route('discussion.update', ['id' => $discussion->id ]) }}" method="post">
                        {{ csrf_field() }}


                        <div class="form-group">
                            <label for="content">Faça uma pergunta</label>
                            <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ $discussion->content }}</textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success float-right" type="submit">Salvar mudanças</button>
                        </div>
                        </form>

                </div>
            </div>
        </div>
@endsection
