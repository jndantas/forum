@extends('layouts.app')

@section('content')

<div class="col-md-8">
    <div class="card" style="margin-bottom:1%">
        <div class="card card-default">
            <div class="card-header">
                <img src="{{ $d->user->avatar }}" alt="" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                <span> {{ $d->user->name }} <b>({{ $d->user->points }}) </b></span>
                @if ($d->hasBestAnswer())
                <span class="btn float-right btn-success btn-sm">Fechado</span>
                @else
                <span class="btn float-right btn-info btn-sm">Aberto</span>
                @endif

                @if (Auth::id() == $d->user->id)

                @if ($d->hasBestAnswer())
                <a href="{{ route('discussion.edit', ['slug' => $d->slug ]) }}" class="btn btn-info btn-sm float-right" style="margin-right: 8px">Editar</a>
                @endif

                @endif

                @if ($d->is_being_watched_by_auth_user())
                    <a href="{{ route('discussion.unwatch', ['id' => $d->id ]) }}" class="btn btn-secondary btn-sm float-right" style="margin-right: 8px">Desver</a>
                @else
                    <a href="{{ route('discussion.watch', ['id' => $d->id ]) }}" class="btn btn-secondary btn-sm float-right" style="margin-right: 8px">Visualizado</a>
                @endif
            </div>
            <div class="card-body">
                <h4 class="text-center">
                    {{ $d->title }}
                </h4>
            <hr>
            <p class="text-center">
                {!! Markdown::convertToHtml($d->content) !!}
            </p>
            <hr>
            @if ($best_answer)
                <div class="text-center" style="padding: 40px;">
                    <h3 class="text-center">
                        Melhor Resposta
                    </h3>
                    <div class="card">
                        <div class="card-header bg-success">
                            <img src="{{ $best_answer->user->avatar }}" alt="" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                            <span>{{ $best_answer->user->name }} <b>( {{ $best_answer->user->points }} )</b></span>
                        </div>
                        <div class="card-body">
                                {!! Markdown::convertToHtml($best_answer->content) !!}
                        </div>
                    </div>
                </div>
            @endif
            </div>
            <div class="card-footer">
                <span>
                        {{ $d->replies->count() }} Respostas
                </span>

                <a href="{{ route('channel', ['slug' => $d->channel->slug ]) }}" class="float-right btn btn-secondary btn-sm">{{ $d->channel->title }}</a>
            </div>
        </div>
    </div>


    @foreach ($d->replies as $r)

        <div class="card card-default">
            <div class="card-header">
                <img src="{{ $r->user->avatar }}" alt="" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                <span> {{ $r->user->name }} <b> ({{ $r->user->points }}) </b></span>
                @if (!$best_answer)
                    @if (Auth::id() == $d->user->id)
                        <a href="{{ route('discussion.best.answer', ['id' => $r->id]) }}" class="btn btn-sm btn-info float-right">Marque a melhor resposta</a>
                    @endif

                @endif

                @if (Auth::id() == $r->user->id)
                    @if (!$r->best_answer)
                    <a href="{{ route('reply.edit', ['id' => $r->id]) }}" class="btn btn-sm btn-info float-right" style="margin-right:9px;">Edit</a>

                    @endif

                @endif
            </div>
            <div class="card-body">
                <p class="text-center">
                    {!! Markdown::convertToHtml($r->content) !!}
                </p>
            </div>
            <div class="card-footer">
                <p>
                    @if ($r->is_liked_by_auth_user())
                        <a href="{{ route('reply.unlike', ['id' => $r->id ]) }}" class="btn btn-danger btn-sm">Descurtir {{ $r->likes->count() }}</a>
                    @else
                        <a href="{{ route('reply.like', ['id' => $r->id ]) }}" class="btn btn-success btn-sm">Curtir <span class="badge">{{ $r->likes->count() }}</span> </a>
                    @endif
                </p>
            </div>
        </div>

    @endforeach

    <div class="card card-default">
        <div class="card-body">

            @if (Auth::check())

            <form action="{{ route('discussion.reply', ['id' => $d->id]) }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <textarea name="reply" id="reply" cols="90" rows="10"></textarea>
                </div>
                <div class="form-group">
                    <button class="float-right">Deixe uma resposta</button>
                </div>

            </form>

            @else
                <div class="text-center">
                    <h2>Faça login para responder</h2>
                </div>
            @endif

        </div>
    </div>




</div>

@endsection
