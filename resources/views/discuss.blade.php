@extends('layouts.app')

@section('content')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Criar uma nova Discursão</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <form action="{{ route('discussion.store')}}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" value="{{ old('title') }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="channel">Escolha um canal</label>
                            <select name="channel_id" id="channel_id" class="form-control">
                                @foreach ($channels as $channel)
                                <option value="{{ $channel->id }}">{{ $channel->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="content">Faça uma pergunta</label>
                            <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ old('content') }}</textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success float-right" type="submit">Criar Discussão</button>
                        </div>
                        </form>

                </div>
            </div>
        </div>
@endsection
