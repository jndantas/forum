@extends('layouts.app')

@section('content')

<div class="col-md-8">
        @foreach ($discussions as $d)
        <div class="card" style="margin-bottom:1%">

        <div class="card card-default">
            <div class="card-header">
                <img src="{{ $d->user->avatar }}" alt="" width="40px" height="40px">&nbsp;&nbsp;&nbsp;
                <span> {{ $d->user->name }}, <b>{{ $d->created_at->diffForHumans() }} </b></span>
                @if ($d->hasBestAnswer())
                <span class="btn float-right btn-success btn-sm">Fechado</span>
                @else
                <span class="btn float-right btn-info btn-sm">Aberto</span>
                @endif
                <a href="{{ route('discussion', ['slug' => $d->slug ]) }}" class="btn btn-secondary btn-sm float-right" style="margin-right: 8px">Ver</a>
            </div>
            <div class="card-body">
                <h4 class="text-center">
                {{ $d->title }}
            </h4>
            <p class="text-center">
                {{ str_limit($d->content, 50)}}
            </p>
            </div>
            <div class="card-footer">
                <span>
                    {{ $d->replies->count() }} Respostas
                </span>

                <a href="{{ route('channel', ['slug' => $d->channel->slug ]) }}" class="float-right btn btn-secondary btn-sm">{{ $d->channel->title }}</a>
            </div>
        </div>

        </div>
        @endforeach
        <div class="text-center">
                {{ $discussions->links()}}
            </div>
</div>




@endsection
