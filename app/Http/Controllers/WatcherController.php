<?php

namespace App\Http\Controllers;
use App\Watcher;
use Auth;
use Session;
use Toastr;
use Illuminate\Http\Request;

class WatcherController extends Controller
{
    public function watch($id)
    {
        Watcher::create([
            'discussion_id' => $id,
            'user_id' => Auth::id()
        ]);

        Session::flash('success', 'Você visualizou essa discussão');
        Toastr::success('Você visualizou essa discussão');

        return redirect()->back();
    }

    public function unwatch($id)
    {
        $watcher = Watcher::where('discussion_id', $id)->where('user_id', Auth::id());
        $watcher->delete();

        Toastr::success('Discussão marcada como não vista');

        return redirect()->back();
    }
}
